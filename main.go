/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

*/
package main

import "gitlab.com/djieno/tikcli/cmd"

func main() {
	cmd.Execute()
}
