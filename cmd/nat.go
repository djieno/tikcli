/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

*/
package cmd

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/spf13/cobra"
)

// tikcliCmd represents the nat command
var tikcliCmd = &cobra.Command{
	Use:   "nat",
	Short: "Update mikrotik nat rules for http/https traffic",
	Long:  `Update existing mikrotik nat rules to ensure http/https traffic is forwarded to the correct ip`,
	Run: func(cmd *cobra.Command, args []string) {
		// setNatHTTP("https://10.1.1.1/rest/ip/firewall/nat/*11")
		userName, _ := cmd.Flags().GetString("username")
		password, _ := cmd.Flags().GetString("password")
		toAddresses, _ := cmd.Flags().GetString("to-addresses")
		id, _ := cmd.Flags().GetString("id")
		ip, _ := cmd.Flags().GetString("ip")
		// Action, _ := cmd.Flags().GetString("action")
		setNatHTTP(userName, password, toAddresses, id, ip)
	},
}

func init() {
	rootCmd.AddCommand(tikcliCmd)

	tikcliCmd.PersistentFlags().String("to-addresses", "", "Ip addresses where the request is masqueraded to")
	tikcliCmd.PersistentFlags().String("username", "", "Username to login to routeros host")
	tikcliCmd.PersistentFlags().String("password", "", "Password to login to routeros host")
	tikcliCmd.PersistentFlags().String("id", "", "Id of the nat rule")
	tikcliCmd.PersistentFlags().String("ip", "", "Ip address of routeros host")
	// natCmd.PersistentFlags().String("action", "", "Action to nat")

}

func setNatHTTP(userName, password, toAddresses, id, ip string) []byte {
	url := "https://" + ip + "/rest/ip/firewall/nat/*" + id
	payload, err := json.Marshal(map[string]interface{}{
		// ".id":          "*10",
		// "action":       "dst-nat",
		// "chain":        "dstnat",
		"comment": "http-test",
		// "disabled":     "false",--id
		// "log":          "false",
		// "log-prefix":   "",
		"to-addresses": toAddresses,
	})
	if err != nil {
		log.Fatal(err)
	}

	//define client for self signed routeros cert to be not verified
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	client := &http.Client{Transport: tr}

	request, err := http.NewRequest(http.MethodPatch, url, bytes.NewBuffer(payload))

	if err != nil {
		log.Printf("Could not do request to mikrotik router. %v", err)
	}

	request.Header.Add("Content-Type", "application/json")
	request.Header.Add("User-Agent", "Go-tp-client/2.0")
	request.SetBasicAuth(userName, password)
	// fmt.Println(url)

	response, err := client.Do(request)
	if err != nil {
		log.Printf("Could not make a request. %v", err)
	}
	defer response.Body.Close()

	responseBytes, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Printf("Could not read response body. %v", err)
	}

	fmt.Println(string(responseBytes))
	return responseBytes
}
